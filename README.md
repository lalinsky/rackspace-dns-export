# Export Rackspace Cloud DNS domain to a zone file

Rackspace Cloud DNS does not support exporting a domain from the web
interface, but it does from the API. This is a simple script that
does just that and nothing else, without using any non-standard
libraries.

Log in to the Cloud Control Panel, go to Account > Account Settings
and there you will find your username and API key.

    python rackspace_dns_export.py -u USERNAME -a APIKEY -d DOMAIN