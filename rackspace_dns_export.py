import sys
import json
import optparse
import urllib2
import time

parser = optparse.OptionParser()
parser.add_option("-u", "--username")
parser.add_option("-a", "--api-key")
parser.add_option("-d", "--domain")
options, args = parser.parse_args()

auth = {
    "auth": {
        "RAX-KSKEY:apiKeyCredentials": {
            "username": options.username,
            "apiKey": options.api_key,
        }
    }
}

print >>sys.stderr, "Authenticating..."
req = urllib2.Request('https://identity.api.rackspacecloud.com/v2.0/tokens', json.dumps(auth))
req.add_header("Content-Type", "application/json")
req.add_header("Accept", "application/json")
tokens = json.loads(urllib2.urlopen(req).read())

token = tokens["access"]["token"]["id"]
for service in tokens["access"]["serviceCatalog"]:
    if service["type"] == "rax:dns":
        endpoint = service["endpoints"][0]
        break
else:
    print >>sys.stderr, "CloudDNS API endpoint not found"
    sys.exit(1)

print >>sys.stderr, "Listing domains..."
req = urllib2.Request(endpoint["publicURL"] + "/domains")
req.add_header("X-Auth-Token", token)
req.add_header("Accept", "application/json")
domains = json.loads(urllib2.urlopen(req).read())

for domain in domains["domains"]:
    if domain["name"] == options.domain:
        break
else:
    print >>sys.stderr, "Domain not found"
    sys.exit(2)

print >>sys.stderr, "Exporting domain..."
req = urllib2.Request(endpoint["publicURL"] + "/domains/{0}/export".format(domain["id"]))
req.add_header("X-Auth-Token", token)
req.add_header("Accept", "application/json")
job = json.loads(urllib2.urlopen(req).read())

attempts = 1
while True:
    print >>sys.stderr, "Waiting for the export to finish... ({0})".format(attempts)
    attempts += 1
    time.sleep(1)
    req = urllib2.Request(job["callbackUrl"] + "?showDetails=true")
    req.add_header("X-Auth-Token", token)
    req.add_header("Accept", "application/json")
    job = json.loads(urllib2.urlopen(req).read())
    if job["status"] == "COMPLETED" or attempts > 60:
        break

if job["status"] == "COMPLETED":
    print job["response"]["contents"]
else:
    print >>sys.stderr, "Export not finished, check this URL later:"
    print >>sys.stderr, "curl -H 'X-Auth-Token: {}' '{}'".format(token, job["callbackUrl"] + "?showDetails=true")
    sys.exit(3)

